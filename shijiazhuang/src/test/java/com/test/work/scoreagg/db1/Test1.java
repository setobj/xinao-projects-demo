package com.test.work.scoreagg.db1;

import cn.hutool.core.date.DateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.alibaba.fastjson2.JSONObject;
import com.test.work.scoreagg.eums.EvalPeriodTypeEnum;
import com.test.work.scoreagg.pojo.db1.JobExecutionResult;

/**
 * @Description
 * @Author GL
 * @Date 2024/8/23
 */
public class Test1 {
    public static void main(String[] args) throws ParseException {

        String timeString = "2024-08-24";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date effectiveDate = sdf.parse(timeString);


        String evalPeriodType = "WK";
        executeJobMethod(effectiveDate,evalPeriodType);

    }

    /**
     *@ApiModelProperty(value = "生效日期")
     * @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
     * @ApiModelProperty(value = "评价周期类型，WK-每周，MO-每 月，QTR-每季度")
     *
     * 生效日期后的一周 比如2024.8.23生效，则一周周 2024年8月30号
     * */
    private static JobExecutionResult executeJobMethod(Date effectiveDate, String evalPeriodType) {
        JobExecutionResult result = new JobExecutionResult();


        String timeString = "2024-08-31 23:59:59";
        Date todayDate = DateUtil.parseDate(timeString);

        //判断两个日期是否是同一天,例如，DateUtil.isSameDay(date1, date2)将返回一个布尔值，表示date1和date2是否是同一天
        if (DateUtil.isSameDay(effectiveDate, todayDate)) {
            result.setShouldExecute(Boolean.FALSE);
            return result;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(effectiveDate);
        //获取当天日期 23
        int effectiveDay = cal.get(Calendar.DAY_OF_MONTH);

        Calendar today = Calendar.getInstance();
        today.setTime(todayDate);
        //获取当天日期 23
        int todayDay = today.get(Calendar.DAY_OF_MONTH);

        if (EvalPeriodTypeEnum.MO.getKey().equals(evalPeriodType)) {
            // 每月
            result.setShouldExecute(todayDay == effectiveDay);
            if (result.getShouldExecute()) {
                today.add(Calendar.MONTH, -1);
                today.set(Calendar.HOUR_OF_DAY, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MILLISECOND, 0);
                result.setCycleStartDate(today.getTime());
            }
        } else if (EvalPeriodTypeEnum.WK.getKey().equals(evalPeriodType)) {
            // 每周，一周的第几天
            int effectiveWeek = cal.get(Calendar.DAY_OF_WEEK);
            int todayWeek = today.get(Calendar.DAY_OF_WEEK);
            result.setShouldExecute(todayWeek == effectiveWeek);
            if (result.getShouldExecute()) {
                today.add(Calendar.WEEK_OF_YEAR, -1);
                today.set(Calendar.HOUR_OF_DAY, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MILLISECOND, 0);
                result.setCycleStartDate(today.getTime());
            }
        } else if (EvalPeriodTypeEnum.QTR.getKey().equals(evalPeriodType)) {
            // 每季度
            int effectiveMonth = cal.get(Calendar.MONTH);
            int todayMonth = today.get(Calendar.MONTH);
            result.setShouldExecute(todayDay == effectiveDay && (todayMonth - effectiveMonth) % 3 == 0);
            if (result.getShouldExecute()) {
                today.add(Calendar.MONTH, -3);
                today.set(Calendar.HOUR_OF_DAY, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MILLISECOND, 0);
                result.setCycleStartDate(today.getTime());
            }
        } else {
            result.setShouldExecute(false);
        }
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.setTime(todayDate);
        todayEnd.add(Calendar.DAY_OF_YEAR,-1);
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        System.out.println(simpleDateFormat.format(todayEnd.getTime()));//将日期转换为字符串

        result.setCycleEndDate(todayEnd.getTime());
        System.out.println(JSONObject.toJSONString(result));
        return result;
    }
}