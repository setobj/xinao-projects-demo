package com.test.work.scoreagg.db1;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson2.JSONObject;
import com.test.work.scoreagg.pojo.db1.MongoObjectModel;
import com.test.work.scoreagg.pojo.db1.MongoQuotaRuleModel;
import com.test.work.scoreagg.service.db1.EvalCompanyResultService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class EvalCompanyResultServiceTest {

    @Autowired
    private EvalCompanyResultService evalCompanyResultService;

    @Autowired
    MongoTemplate mongoTemplate;

    @Test
    public void test(){


        //比如筛选city_code字段等于10000010的文档
        Criteria criteria = Criteria.where("city_code").is("10000010");
        // Match stage
        MatchOperation matchOperation = Aggregation.match(criteria);

        // Group stage 将集合中的文档分组，用于统计集合
        GroupOperation groupOperation = Aggregation.group(
                "county_code", "county_name", "object_code", "object_name", "object_type", "L1_quota_group_code", "L1_quota_group_name"
        ).sum("quota_value").as("quotaGroupValue");

        // Project stage 修改输入文档的结构
        ProjectionOperation projectionOperation = Aggregation.project()
                .and("$_id.county_code").as("countyCode")
                .and("$_id.county_name").as("countyName")
                .and("$_id.object_code").as("objectCode")
                .and("$_id.object_name").as("objectName")
                .and("$_id.object_type").as("objectType")
                .and("$_id.L1_quota_group_code").as("quotaGroupCode")
                .and("$_id.L1_quota_group_name").as("quotaGroupName")
                .and("quotaGroupValue").as("quotaGroupValue")
                .andExclude("_id");

        // Create aggregation pipeline
        Aggregation aggregation = Aggregation.newAggregation(
                matchOperation,
                groupOperation,
                projectionOperation
        );
        List<MongoObjectModel> evalCompanyResult = mongoTemplate.aggregate(aggregation, "eval_company_result", MongoObjectModel.class).getMappedResults();
        System.out.println(JSONObject.toJSONString(evalCompanyResult));
        System.out.println("evalCompanyResult："+evalCompanyResult.size());

        List<MongoQuotaRuleModel> mongoScore = getMongoScore(evalCompanyResult);
        System.out.println("mongoScore："+JSONObject.toJSONString(mongoScore));

    }



    public List<MongoQuotaRuleModel> getMongoScore(List<MongoObjectModel> models) {
        List<MongoQuotaRuleModel> list = new ArrayList<>();
        Map<String, Map<String, MongoQuotaRuleModel>> maps = MongoQuotaRuleModel.getMongoQuotaRuleMap();
        if (CollUtil.isEmpty(models)) {
            return list;
        }

        log.info("maps1=" + JSONObject.toJSONString(maps));

        //将查询结果处理到中间map中
        for (MongoObjectModel item : models) {
            //根据分组名称，拿到当前分组的数据
            Map<String, MongoQuotaRuleModel> ruleMap = MapUtil.get(maps, item.getQuotaGroupName(), Map.class);
            if (ObjectUtil.isNotEmpty(ruleMap)) {
                for (Map.Entry<String, MongoQuotaRuleModel> entry : ruleMap.entrySet()) {
                    if (item.getObjectType().contains(entry.getKey())) {
                        MongoQuotaRuleModel model = entry.getValue();
                        model.setEntCount(model.getEntCount() + 1);
                        model.setScore(model.getScore().add(item.getQuotaGroupValue()));
                    }
                }
            }
        }

        log.info("maps=2" + JSONObject.toJSONString(maps));


        //遍历结果map，获取最后四个维度得分
        for (Map.Entry<String, Map<String, MongoQuotaRuleModel>> outerEntry : maps.entrySet()) {
            String outerKey = outerEntry.getKey();
            Map<String, MongoQuotaRuleModel> innerMap = outerEntry.getValue();
            MongoQuotaRuleModel model = new MongoQuotaRuleModel();

            model.setName(outerKey);
            for (Map.Entry<String, MongoQuotaRuleModel> innerEntry : innerMap.entrySet()) {
                MongoQuotaRuleModel modelKey = innerEntry.getValue();
                if (modelKey.getEntCount() > 0) {
                    BigDecimal result = modelKey.getScore().divide(BigDecimal.valueOf(modelKey.getEntCount()), 2, RoundingMode.HALF_UP).multiply(modelKey.getRate()).setScale(2, RoundingMode.HALF_UP);
                    model.setScore(model.getScore().add(result).setScale(2, RoundingMode.HALF_UP));
                }
            }
            list.add(model);
        }

        System.out.println("list："+JSONObject.toJSONString(list));

        return list;
    }


    @Test
    public void test2(){
        evalCompanyResultService.getEvalCompResuList("2236055936130b001354");
    }

}
