package com.test.work;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;

/**
 * @Description:
 * @Date: 2024/8/14 17:24
 * @Author: zhangguoliang
 */
@SpringBootApplication
@Slf4j
public class ShijiazhuangApplication {


    public static void main(String[] args) throws Exception{
       ConfigurableApplicationContext application = SpringApplication.run(ShijiazhuangApplication.class, args);

        Environment env = application.getEnvironment();
        log.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Login: \thttp://{}:{}/sjz\n\t" +
                        "----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"));

        /*try {
            SpringApplication.run(ShijiazhuangApplication.class, args);
            System.out.println("-----------------> mybatis plus 工具类启动成功");
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

}
