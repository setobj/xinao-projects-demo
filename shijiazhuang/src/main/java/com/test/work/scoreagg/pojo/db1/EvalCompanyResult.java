package com.test.work.scoreagg.pojo.db1;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import java.util.Date;

/**
 * 企业评价规则，评价记录
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("eval_company_result")
public class EvalCompanyResult {

    /** ID*/
    @Id
    public String id;


    /** 规则名称*/
    @Field(name = "model_name")
    public String modelName;

    /** 规则编码*/
    @Field(name = "model_code")
    public String modelCode;

    /** 规则是否人工审核 Y是，N不是*/
    @Field(name = "model_manual_approval")
    public String modelManualApproval;

    /** ISODate("2024-07-25T09:40:57.316Z")*/
    @Field(name = "collect_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date collectTime;

    /** 模型编码_时间戳批次唯一 2420636615c73805RSo_1721900457173*/
    @Field(name = "collect_batch")
    public String collectBatch;

    /** 省编码*/
    @Field(name = "province_code")
    public String provinceCode;

    /** 省名称*/
    @Field(name = "province_name")
    public String provinceName;

    /** 市编码*/
    @Field(name = "city_code")
    public String cityCode;

    /** 市名称*/
    @Field(name = "city_name")
    public String cityName;

    /** 区编码*/
    @Field(name = "county_code")
    public String countyCode;

    /** 区名称*/
    @Field(name = "county_name")
    public String countyName;

    /** 评价对象对象名称,企业名称*/
    @Field(name = "object_name")
    public String objectName;

    /** 对象唯一编码，不同对象不同，关联企业的uuid*/
    @Field(name = "object_code")
    public String objectCode;

    /** 评价周期类型，WK-每周，MO-每 月，QTR-每季度*/
    @Field(name = "eval_period_type")
    public String evalPeriodType;

    /** 生效日期*/
    @Field(name = "effective_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date effectiveDate;

    /** 预警对象，WEAK_PIPE_Q-薄弱企业管道气,WEAK_BOTTLE_Q-薄弱企业瓶装气,WEAK_CAR_Q-薄弱企业汽车加气,WEAK_LIQ_NON_RESIDENT-薄弱用户液化气非居民用户,WEAK_RESIDENT-薄弱用户居民用户,WEAK_PIP_NON_RESIDENT-薄弱用户管道气非居民用户,WEAK_AREA-薄弱区区域,WEAK_AREA-薄弱区区域*/
    @Field(name = "object_type")
    public String objectType;

    /** 可能有多级指标分组,本期没有*/
    @Field(name = "L1_quota_group_name")
    public String L1QuotaGroupName;

    /** QG_01*/
    @Field(name = "L1_quota_group_code")
    public String L1QuotaGroupCode;

    /** */
    @Field(name="L1_quota_group_value")
    public Double L1_quota_group_value;

    /** 测试指标名称*/
    @Field(name = "quota_name")
    public String quotaName;


    /** 预警状态，0-未预警默认，1-上限发⽣预警,-1下限发⽣预警*/
    @Field(name = "quota_status")
    public Integer quotaStatus;

    /** 测试指标分数*/
    @Field(name = "quota_value")
    public Double quotaValue;

    /** 运营指标描述*/
    @Field(name = "quota_operate_running")
    public String quotaOperateRunning;

    /** 指标系数*/
    @Field(name = "quotaWeight")
    public Integer quota_weight;

    /** 指标得分下限制*/
    @Field(name = "quotaDownLimit")
    public Integer quota_down_limit;

    /** 指标得分上限制*/
    @Field(name = "quota_up_limit")
    public String quotaUpLimit;

    /** 指标上限阈值*/
    @Field(name = "quota_up_threshold")
    public String quotaUpThreshold;

    /** 指标下限阀值*/
    @Field(name = "quota_down_threshold")
    public String quotaDownThreshold;


    /** 跑批批次*/
    @Field(name = "result_batch")
    public String resultBatch;


    /** 创建时间*/
    @Field(name = "create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date createTime;

    /** 周期开始时间*/
    @Field(name = "cycle_start_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date cycleStartTime;

    /**  周期结束时间*/
    @Field(name = "cycle_end_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date cycleEndTime;

    /** 考核评价状态 已完成 未完成*/
    @Field(name = "eval_status")
    public String evalStatus;

    /** 更新时间*/
    @Field(name = "update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date updateTime;

    /** 总得分，对应评价记录-已评价-评分*/
    @Field(name = "object_value")
    public Double objectValue;





}
