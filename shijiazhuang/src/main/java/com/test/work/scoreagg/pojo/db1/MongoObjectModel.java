package com.test.work.scoreagg.pojo.db1;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class MongoObjectModel {

    private String countyCode;

    private String countyName;

    private String objectCode;

    private String objectName;

    private String objectType;

    private String quotaGroupCode;

    private String quotaGroupName;

    private BigDecimal quotaGroupValue;

}