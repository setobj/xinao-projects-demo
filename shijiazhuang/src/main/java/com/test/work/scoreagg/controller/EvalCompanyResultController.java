package com.test.work.scoreagg.controller;

import com.test.work.common.entity.R;
import com.test.work.scoreagg.pojo.db1.EvalCompanyResult;
import com.test.work.scoreagg.service.db1.EvalCompanyResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/evalCompanyResult")
public class EvalCompanyResultController {

    @Autowired
    private EvalCompanyResultService evalCompanyResultService;

    @GetMapping("/getEvalCompanyResultList")
    public R<List<EvalCompanyResult>> getEvalCompanyResultList(){
        List<EvalCompanyResult> evalCompanyResultList = evalCompanyResultService.getEvalCompanyResultList();
        return R.success(evalCompanyResultList);
    }

}
