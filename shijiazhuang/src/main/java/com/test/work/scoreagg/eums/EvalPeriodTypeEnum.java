package com.test.work.scoreagg.eums;

import lombok.Getter;

@Getter
public enum EvalPeriodTypeEnum {

    /**
     * 每周一次（七天）
     */
    WK("WK", "每周一次（七天）"),
    /**
     * 每月一次（自然月）
     */
    MO("MO", "每月一次（自然月）"),
    /**
     * 每季度一次（自然月）
     */
    QTR("QTR", "每季度一次（自然月）");



    private final String key;
    private final String val;


    EvalPeriodTypeEnum(String key, String val) {
        this.key = key;
        this.val = val;
    }

    public static EvalPeriodTypeEnum getEnumByKey(String key){
        for (EvalPeriodTypeEnum gasEnum:values()){
            if (gasEnum.key.equals(key)){
                return gasEnum;
            }
        }
        return null;
    }

    public static EvalPeriodTypeEnum getEnumByVal(String val){
        for (EvalPeriodTypeEnum gasEnum:values()){
            if (gasEnum.val.equals(val)){
                return gasEnum;
            }
        }
        return null;
    }
}
