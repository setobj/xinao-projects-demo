package com.test.work.scoreagg.service.db1;

import com.test.work.scoreagg.pojo.db1.EvalCompanyResult;
import com.test.work.scoreagg.pojo.db1.EvalCompanyResultDto;
import com.test.work.scoreagg.pojo.db1.MongoObjectModel;

import java.util.List;

public interface EvalCompanyResultService {

    List<EvalCompanyResult> getEvalCompanyResultList();


    /**
     * 根据区县、企业、一级分组聚合查询
     *
     * @param collectBatches
     * @param quotaGroupNameList "运行保障", "安全管控", "居民满意", "设备在线"
     * @param objectCode
     * @param countyCode
     * @param countyCodeList
     * @return
     */
   List<MongoObjectModel> getMongoObjectData(List<String> collectBatches,
                                                     List<String> quotaGroupNameList,
                                                     String objectCode,
                                                     String countyCode,
                                                     List<String> countyCodeList);


    /**
     *   查询企业评价明细
     *   objectCode 企业唯一编码
     * */
    List<EvalCompanyResultDto> getEvalCompResuList(String objectCode);
}
