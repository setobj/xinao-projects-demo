package com.test.work.scoreagg.pojo.db1;

import lombok.Data;

/**
 * @Description
 * @Author GL
 * @Date 2024/8/24
 */
@Data
public class EvalCompanyResultDto {

    public String id;

    public String objectName;

    public String quotaName;

    public String quotaOperateRunning;

    public String quotaoValue;
}