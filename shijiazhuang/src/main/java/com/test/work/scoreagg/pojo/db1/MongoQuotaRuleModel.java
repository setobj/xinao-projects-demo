package com.test.work.scoreagg.pojo.db1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author damin
 * @createTime 2024/7/24 21:29
 * @desc 指标规则实体定义 先弃用
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MongoQuotaRuleModel {

    private String name;

    private String code;

    //总分
    private BigDecimal score = new BigDecimal(0);

    //权重
    private BigDecimal rate;

    //企业数量
    private Integer entCount = 0;

    public MongoQuotaRuleModel(String name, String code, BigDecimal rate) {
        this.name = name;
        this.code = code;
        this.rate = rate;
    }


    public static Map<String, Map<String, MongoQuotaRuleModel>> getMongoQuotaRuleMap() {
        Map<String, Map<String, MongoQuotaRuleModel>> map = new HashMap<>();
        MongoQuotaRuleModel pipeRule =
                new MongoQuotaRuleModel("管道", "WEAK_PIPE_Q", new BigDecimal("0.4"));
        MongoQuotaRuleModel bottleRule =
                new MongoQuotaRuleModel("瓶装", "WEAK_BOTTLE_Q", new BigDecimal("0.4"));
        MongoQuotaRuleModel carRule =
                new MongoQuotaRuleModel("加气", "WEAK_CAR_Q", new BigDecimal("0.2"));

        MongoQuotaRuleModel pipeRule1 =
                new MongoQuotaRuleModel("管道", "WEAK_PIPE_Q", new BigDecimal("0.4"));
        MongoQuotaRuleModel bottleRule1 =
                new MongoQuotaRuleModel("瓶装", "WEAK_BOTTLE_Q", new BigDecimal("0.4"));
        MongoQuotaRuleModel carRule1 =
                new MongoQuotaRuleModel("加气", "WEAK_CAR_Q", new BigDecimal("0.2"));

        MongoQuotaRuleModel pipeRule2 =
                new MongoQuotaRuleModel("管道", "WEAK_PIPE_Q", new BigDecimal("0.4"));
        MongoQuotaRuleModel bottleRule2 =
                new MongoQuotaRuleModel("瓶装", "WEAK_BOTTLE_Q", new BigDecimal("0.4"));
        MongoQuotaRuleModel carRule2 =
                new MongoQuotaRuleModel("加气", "WEAK_CAR_Q", new BigDecimal("0.2"));

        MongoQuotaRuleModel pipeRule3 =
                new MongoQuotaRuleModel("管道", "WEAK_PIPE_Q", new BigDecimal("0.6"));
        MongoQuotaRuleModel bottleRule3 =
                new MongoQuotaRuleModel("瓶装", "WEAK_BOTTLE_Q", new BigDecimal("0.4"));

        Map<String, MongoQuotaRuleModel> yxMap = new HashMap<>();
        Map<String, MongoQuotaRuleModel> gkMap = new HashMap<>();
        Map<String, MongoQuotaRuleModel> sbMap = new HashMap<>();
        Map<String, MongoQuotaRuleModel> jmMap = new HashMap<>();

        yxMap.put("WEAK_PIPE_Q", pipeRule);
        yxMap.put("WEAK_BOTTLE_Q", bottleRule);
        yxMap.put("WEAK_CAR_Q", carRule);

        gkMap.put("WEAK_PIPE_Q", pipeRule1);
        gkMap.put("WEAK_BOTTLE_Q", bottleRule1);
        gkMap.put("WEAK_CAR_Q", carRule1);

        sbMap.put("WEAK_PIPE_Q", pipeRule2);
        sbMap.put("WEAK_BOTTLE_Q", bottleRule2);
        sbMap.put("WEAK_CAR_Q", carRule2);

        jmMap.put("WEAK_PIPE_Q", pipeRule3);
        jmMap.put("WEAK_BOTTLE_Q", bottleRule3);

        map.put("运行保障", yxMap);
        map.put("安全管控", gkMap);
        map.put("设备在线", sbMap);
        map.put("居民满意", jmMap);

        //        map.put("运行安全", yxMap);
        //        map.put("企业安全", gkMap);
        //        map.put("物联安全", sbMap);
        //        map.put("测试安全", jmMap);

        return map;
    }


}
