package com.test.work.datahandler.mapper.db1;

import com.test.work.datahandler.entity.db1.MeshBtUserExt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 瓶装气用户扩展表 Mapper 接口
 * </p>
 *
 * @author zgl
 * @since 2024-08-14
 */
public interface MeshBtUserExtMapper extends BaseMapper<MeshBtUserExt> {

}
