package com.test.work.datahandler.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.test.work.datahandler.entity.ImportExcelDataDto;
import com.test.work.datahandler.entity.db1.DeviceInfoV2;
import com.test.work.datahandler.entity.db2.MeshBtUserExt;
import com.test.work.datahandler.service.SjzDataHandlerService;
import com.test.work.datahandler.service.db1.DeviceInfoV2Service;
import com.test.work.datahandler.service.db2.MeshBtUserExtService2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Date: 2024/8/15 11:16
 * @Author: zhangguoliang
 */
@Service
@Slf4j
public class SjzDataHandlerServiceImpl implements SjzDataHandlerService {

    @Autowired(required = false)
    private DeviceInfoV2Service deviceInfoV2Service;

    @Autowired
    private MeshBtUserExtService2 meshBtUserExtService2;


    /***
     * @Description 根据excel中的设备编码(最后一列)，从设备信息表device_info_v2中查询，
     * 如果查询到，则根据excel中的 商户名称 去查询商户信息表mesh_bt_user_ext
     * 如果有，则device_info_v2 monitor_obj_type='BT',monitor_obj_key=商户ID
     * monitor_obj_type监测对象类型,BT: 瓶装气用户
     *         则mesh_bt_user_ext has_alarm_device = 1 and alarm_device_status=NORMAL
     * has_alarm_device是否安装燃气报警器 alarm_device_status
     * 报警器状态,NORMAL-正常安装, NONE-无报警器, NO_CODE-已安装,无法识别编号
     *
     * @param dataList
     * @Auth
     * @Date 2024/8/15 11:29
     *
     */
    @Override
    public void matchHandlerData(List<ImportExcelDataDto> dataList) {
        List<String> deviceCodes = dataList.stream().map(ImportExcelDataDto::getSheBeiBianHao).collect(Collectors.toList());
        log.info("excel数据处理-所有设备编码："+ JSONObject.toJSONString(deviceCodes));

        LambdaQueryWrapper<DeviceInfoV2> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.in(DeviceInfoV2::getDeviceCode, deviceCodes);
        List<DeviceInfoV2> devlist = deviceInfoV2Service.list(queryWrapper);
        long index1 = System.currentTimeMillis();
        log.info(index1+":excel数据处理,更新设备-更新设备信息之前：{}",JSONObject.toJSONString(devlist));

        List<MeshBtUserExt> upMeshlist = new ArrayList<>();
        List<DeviceInfoV2> upDevicelist = new ArrayList<>();
        if (!CollectionUtils.isEmpty(devlist)) {
            for (int i = 0; i < devlist.size(); i++) {
                DeviceInfoV2 deviceInfoV2 =  devlist.get(i);
                //假设excel中的设备编码不会重复
                List<ImportExcelDataDto> collect = dataList.stream().filter(o -> o.getSheBeiBianHao().equals(deviceInfoV2.getDeviceCode()) ).collect(Collectors.toList());

                //查询商户，并获取一个商户
                ImportExcelDataDto importExcelDataDto =  collect.get(0);
                LambdaQueryWrapper<MeshBtUserExt> query = Wrappers.lambdaQuery();
                query.eq(MeshBtUserExt::getName,importExcelDataDto.getYongHuMingCheng());
                query.eq(MeshBtUserExt::getSysLocDistrictName,importExcelDataDto.getQuXianMingCheng());
                query.eq(MeshBtUserExt::getSysRegionTownName,importExcelDataDto.getJieDaoMingCheng());
                List<MeshBtUserExt> list = meshBtUserExtService2.list(query);

                if (!CollectionUtils.isEmpty(list)) {
                    DeviceInfoV2 curDevice = new DeviceInfoV2();
                    curDevice.setId(deviceInfoV2.getId());
                    Long id = list.get(0).getId();
                    curDevice.setMonitorObjKey(id+"");
                    curDevice.setMonitorObjKey("BT");
                    upMeshlist.addAll(list);
                    upDevicelist.add(curDevice);
                }
            }

        }
        if (!CollectionUtils.isEmpty(upDevicelist)) {
            this.upDevicelist(upDevicelist,index1);
        }
        if (!CollectionUtils.isEmpty(upMeshlist)) {
            long index2 = System.currentTimeMillis();
            log.info(index2+":excel数据处理,更新商户-商户信息更新之前：{}",JSONObject.toJSONString(upMeshlist));
            this.upMeshlist(upMeshlist,index2);
        }
        log.info("excel数据处理-符合条件的MeshBtUserExt信息："+ JSONObject.toJSONString(devlist));

    }

    @Override
    public void upDevicelist(List<DeviceInfoV2> upDevicelist,long index1) {
        boolean b = deviceInfoV2Service.updateBatchById(upDevicelist);
        if (b) {
            log.info(index1+":excel数据处理,更新设备-更新条数：{},更新设备信息之后：{}",upDevicelist.size(),JSONObject.toJSONString(upDevicelist));
        }

    }

    @Override
    public void upMeshlist(List<MeshBtUserExt> upMeshList,long index2) {
        List<MeshBtUserExt> up = new ArrayList<>();
        for (int i = 0; i < upMeshList.size(); i++) {
            MeshBtUserExt meshBtUserExt =  upMeshList.get(i);
            MeshBtUserExt curObj = new MeshBtUserExt();
            curObj.setId(meshBtUserExt.getId());
            curObj.setHasAlarmDevice(1);
            curObj.setAlarmDeviceStatus("NORMAL");
            up.add(curObj);
        }
        boolean b = meshBtUserExtService2.updateBatchById(up);
        if (b) {
            log.info(index2+":excel数据处理,更新商户-：更新条数：{}，更新商户信息之后：{}",up.size(),JSONObject.toJSONString(up));
        }
    }
}
