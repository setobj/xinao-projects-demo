package com.test.work.datahandler.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 * @Date: 2024/8/15 10:46
 * @Author: zhangguoliang
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImportExcelDataDto {

    /** 区县名称*/
    private String quXianMingCheng;

    /** 报警覆盖情况*/
    private String baoJingFuGaiQingKuang;

    /** 街道名称*/
    private String jieDaoMingCheng;

    /** 所属社区(村)*/
    private String suoShuSheQu;

    /** 供气企业*/
    private String gongQiQiYe;

    /** 用户名称，饭店名称*/
    private String yongHuMingCheng;

    /** 负责人 */
    private String fuZeRen;

    /** 联系电话*/
    private String phone;

    /** 所在地址*/
    private String address;

    /** 设备编号*/
    private String sheBeiBianHao;
}
