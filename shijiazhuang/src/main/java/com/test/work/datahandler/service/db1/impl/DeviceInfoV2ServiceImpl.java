package com.test.work.datahandler.service.db1.impl;

import com.test.work.datahandler.entity.db1.DeviceInfoV2;
import com.test.work.datahandler.mapper.db1.DeviceInfoV2Mapper;
import com.test.work.datahandler.service.db1.DeviceInfoV2Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设备信息表 服务实现类
 * </p>
 *
 * @author zgl
 * @since 2024-08-14
 */
@Service
public class DeviceInfoV2ServiceImpl extends ServiceImpl<DeviceInfoV2Mapper, DeviceInfoV2> implements DeviceInfoV2Service {

}
