package com.test.work.datahandler.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 瓶装气用户扩展表 前端控制器
 * </p>
 *
 * @author zgl
 * @since 2024-08-14
 */
@RestController
@RequestMapping("/mesh-bt-user-ext")
public class MeshBtUserExtController {

}

