package com.test.work.datahandler.service.db1.impl;

import com.test.work.datahandler.entity.db1.MeshBtUserExt;
import com.test.work.datahandler.mapper.db1.MeshBtUserExtMapper;
import com.test.work.datahandler.service.db1.MeshBtUserExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 瓶装气用户扩展表 服务实现类
 * </p>
 *
 * @author test
 * @since 2024-08-14
 */
@Service
public class MeshBtUserExtServiceImpl extends ServiceImpl<MeshBtUserExtMapper, MeshBtUserExt> implements MeshBtUserExtService {

}
