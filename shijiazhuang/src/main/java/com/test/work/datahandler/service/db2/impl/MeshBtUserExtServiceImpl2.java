package com.test.work.datahandler.service.db2.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.test.work.datahandler.entity.db2.MeshBtUserExt;
import com.test.work.datahandler.mapper.db2.MeshBtUserExtMapper2;
import com.test.work.datahandler.service.db2.MeshBtUserExtService2;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 瓶装气用户扩展表 服务实现类
 * </p>
 *
 * @author test
 * @since 2024-08-14
 */
@Service
public class MeshBtUserExtServiceImpl2 extends ServiceImpl<MeshBtUserExtMapper2, MeshBtUserExt> implements MeshBtUserExtService2 {


}
