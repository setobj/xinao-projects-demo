package com.test.work.datahandler.service;

import com.test.work.datahandler.entity.ImportExcelDataDto;
import com.test.work.datahandler.entity.db1.DeviceInfoV2;
import com.test.work.datahandler.entity.db2.MeshBtUserExt;

import java.util.List;

/**
 * @Description:
 * @Date: 2024/8/15 11:16
 * @Author: zhangguoliang
 */
public interface SjzDataHandlerService {

     void matchHandlerData(List<ImportExcelDataDto> dataList);

     void upDevicelist(List<DeviceInfoV2> upDevicelist,long index1);

     void upMeshlist(List<MeshBtUserExt> upMeshList,long index2);

}
