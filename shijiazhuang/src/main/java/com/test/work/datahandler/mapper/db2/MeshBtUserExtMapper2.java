package com.test.work.datahandler.mapper.db2;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.test.work.datahandler.entity.db2.MeshBtUserExt;

/**
 * <p>
 * 瓶装气用户扩展表 Mapper 接口
 * </p>
 *
 * @author zgl
 * @since 2024-08-14
 */
public interface MeshBtUserExtMapper2 extends BaseMapper<MeshBtUserExt> {

}
