package com.test.work.datahandler.service.db1;

import com.test.work.datahandler.entity.db1.MeshBtUserExt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 瓶装气用户扩展表 服务类
 * </p>
 *
 * @author zgl
 * @since 2024-08-14
 */
public interface MeshBtUserExtService extends IService<MeshBtUserExt> {

}
