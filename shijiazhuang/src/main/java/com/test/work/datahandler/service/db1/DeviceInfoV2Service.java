package com.test.work.datahandler.service.db1;

import com.test.work.datahandler.entity.db1.DeviceInfoV2;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设备信息表 服务类
 * </p>
 *
 * @author zgl
 * @since 2024-08-14
 */
public interface DeviceInfoV2Service extends IService<DeviceInfoV2> {

}
