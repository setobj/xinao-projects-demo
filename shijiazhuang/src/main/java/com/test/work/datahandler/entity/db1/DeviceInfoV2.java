package com.test.work.datahandler.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 设备信息表
 * </p>
 *
 * @author zgl
 * @since 2024-08-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class DeviceInfoV2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 公司ID
     */
    private Long companyInfoId;

    /**
     * 公司名称
     */
    private String companyInfoName;

    /**
     * 告警来源，IOT/JCYY/ENNEW
     */
    private String deviceSource;

    /**
     * 外部的设备ID/CODE
     */
    private String deviceCode;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 设备二级类型,非必填
     */
    private String deviceSubType;

    /**
     * 设备地址
     */
    private String deviceAddress;

    private String dataType;

    private String address;

    /**
     * 厂站UUID
     */
    private String stationUuid;

    /**
     * 外部站点ID，保留字段
     */
    private String standId;

    /**
     * 厂站名称
     */
    private String stationName;

    /**
     * 设备经度
     */
    private String longitude;

    /**
     * 设备纬度
     */
    private String latitude;

    /**
     * 今日报警总数
     */
    private Integer todayAlarmCount;

    /**
     * 报警总数
     */
    private Integer alarmTotal;

    /**
     * 报警状态（0：正常，1：报警）
     */
    private Boolean alarmStatus;

    /**
     * 设备型号
     */
    private String deviceModel;

    /**
     * 监测指标code
     */
    private String metricCode;

    /**
     * 监测指标
     */
    private String metricName;

    /**
     * 0：表示正常；1：表示逻辑删除，0：默认值
     */
    private Boolean deleteFlag;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 0:不在线；1：在线
     */
    private Integer state;

    /**
     * 使用状态, 在用: NORMAL 停用: DISABLE 故障: ERROR
     */
    private String useStatus;

    /**
     * 监测对象类型,BT: 瓶装气用户
     */
    private String monitorObjType;

    /**
     * 监测对象关联标识/主键
     */
    private String monitorObjKey;

    private String createBy;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    private String updateBy;

    private String unit;

    /**
     * 部门
     */
    private String deptCode;

    /**
     * 设备实际所在的行政区
     */
    private String deviceDeptCode;

    /**
     * 村UUID
     */
    private String villageUuid;

    private String standardCode;

    private String limitMinValue;

    private String limitMaxValue;

    /**
     * 监控对象名称，不一定和monitor_obj_key有关联
     */
    private String monitorObjName;


}
