package com.test.work.datahandler.mapper.db1;

import com.test.work.datahandler.entity.db1.DeviceInfoV2;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设备信息表 Mapper 接口
 * </p>
 *
 * @author zgl
 * @since 2024-08-14
 */
public interface DeviceInfoV2Mapper extends BaseMapper<DeviceInfoV2> {

}
