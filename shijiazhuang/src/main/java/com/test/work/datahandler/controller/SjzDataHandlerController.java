package com.test.work.datahandler.controller;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.test.work.common.utils.ImportExcelUtil;
import com.test.work.datahandler.entity.ImportExcelDataDto;
import com.test.work.datahandler.entity.db1.DeviceInfoV2;
import com.test.work.datahandler.entity.db2.MeshBtUserExt;
import com.test.work.datahandler.service.SjzDataHandlerService;
import com.test.work.datahandler.service.db1.DeviceInfoV2Service;
import com.test.work.datahandler.service.db2.MeshBtUserExtService2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 石家庄数据处理
 * @Date: 2024/8/15 09:55
 * @Author: zhangguoliang
 */
@RestController
@RequestMapping("/dataHand")
@Slf4j
public class SjzDataHandlerController {

    @Autowired
    private DeviceInfoV2Service deviceInfoV2Service;

    @Autowired
    private MeshBtUserExtService2 meshBtUserExtService2;

    @Autowired
    private SjzDataHandlerService sjzDataHandlerService;


    @Value("${spring.profiles.active}")
    private String active;

    @GetMapping("/getActive")
    public String getActive() {
        return active+"-"+System.currentTimeMillis();
    }

    @GetMapping("/dataHand1")
    public String dataHand() {
        LambdaQueryWrapper<DeviceInfoV2> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(DeviceInfoV2::getId, "1");
        List<DeviceInfoV2> list = deviceInfoV2Service.list(queryWrapper);
        list.forEach(deviceInfoV2 -> System.out.println(deviceInfoV2.toString()));


        MeshBtUserExt byId = meshBtUserExtService2.getById("1");
        System.out.println(byId.toString());

        return "success";
    }

    /***
     * @Description 匹配excel中的设备编号
     *                  查询 设备信息表，然后查询用户表，两个表分别同步两个字段
     * @param file
     * @Auth
     * @Date 2024/8/15 10:26
     * @return java.util.Map
     */
    @PostMapping("/matchHandlerData")
    @ResponseBody
    public Map add(@RequestParam(value = "file") MultipartFile file) {
        log.info("匹配excel中的设备编号-开始："+System.currentTimeMillis());

        Map<String,Object> retMap = new HashMap<>();
        retMap.put("active",active);
        retMap.put("msg","更新成功！");
        //判断文件是否空
        if (file == null || file.getOriginalFilename() == null || "".equalsIgnoreCase(file.getOriginalFilename().trim())) {
            retMap.put("msg","文件不能为空");
        }

        try {
            //获取数据
            List<List<Object>> olist = ImportExcelUtil.getListByExcel(file.getInputStream(), file.getOriginalFilename());
            if (!CollectionUtils.isEmpty(olist)) {
                List<ImportExcelDataDto> dataList = new ArrayList<ImportExcelDataDto>();
                for (int i=0;i<olist.size();i++) {
                    List<Object> objects = olist.get(i);
                    if (!CollectionUtils.isEmpty(objects)) {
                        ImportExcelDataDto curObj = new ImportExcelDataDto();
                        curObj.setQuXianMingCheng(ObjectUtils.isEmpty(objects.get(0))?"":objects.get(0).toString());
                        curObj.setBaoJingFuGaiQingKuang(ObjectUtils.isEmpty(objects.get(1))?"":objects.get(1).toString());
                        curObj.setJieDaoMingCheng(ObjectUtils.isEmpty(objects.get(2))?"":objects.get(2).toString());
                        curObj.setSuoShuSheQu(ObjectUtils.isEmpty(objects.get(3))?"":objects.get(3).toString());
                        curObj.setGongQiQiYe(ObjectUtils.isEmpty(objects.get(4))?"":objects.get(4).toString());
                        curObj.setYongHuMingCheng(ObjectUtils.isEmpty(objects.get(5))?"":objects.get(5).toString());
                        curObj.setFuZeRen(ObjectUtils.isEmpty(objects.get(6))?"":objects.get(6).toString());
                        curObj.setPhone(ObjectUtils.isEmpty(objects.get(7))?"":objects.get(7).toString());
                        curObj.setAddress(ObjectUtils.isEmpty(objects.get(8))?"":objects.get(8).toString());
                        curObj.setSheBeiBianHao(ObjectUtils.isEmpty(objects.get(9))?"":objects.get(9).toString());
                        dataList.add(curObj);
                    }
                }
                log.info("匹配excel中的设备编号-excel数据："+ JSONObject.toJSONString(dataList));
                sjzDataHandlerService.matchHandlerData(dataList);
            }
        } catch (Exception e) {
            retMap.put("msg","转换excel数据异常");
            log.error("匹配excel中的设备编号-异常：{}",e);
        }

        log.info("匹配excel中的设备编号-结束："+System.currentTimeMillis());
        return retMap;
    }
}