package com.test.work.datahandler.entity.db1;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 瓶装气用户扩展表
 * </p>
 *
 * @author zgl
 * @since 2024-08-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MeshBtUserExt implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * dept_code
     */
    private String deptCode;

    /**
     * 供气企业，关联本库同步公司表company_info
     */
    private Long companyInfoId;

    /**
     * 关联采集工具单位用户表，类型为商业用户餐饮单位，表为gc_com
     */
    private String refId;

    /**
     * 所属县区
     */
    private Long sysLocDistrictId;

    /**
     * 所属县区名称
     */
    private String sysLocDistrictName;

    /**
     * 所属城镇
     */
    private Long sysRegionTownId;

    /**
     * 所属城镇名称
     */
    private String sysRegionTownName;

    /**
     * 所属社区
     */
    private Long systemLocVillageId;

    /**
     * 所属社区名称
     */
    private String systemLocVillageName;

    /**
     * 用户类型，BU-商业用户，HU-家庭用户
     */
    private String userType;

    /**
     * 商户名称，采集工具中已存在，冗余，同步一
     */
    private String name;

    /**
     * 联系方式， 冗余，同步一份到采集工具
     */
    private String phone;

    /**
     * 联系人
     */
    private String contacts;

    /**
     * 瓶装气使用情况，Y-在用，N-停用
     */
    private String btUsage;

    /**
     * 包联网格员ID
     */
    private Long meshUserId;

    /**
     * 所在地址
     */
    private String address;

    /**
     * 经度
     */
    private String lon;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 门头照片保持路径
     */
    private String headPhoto;

    private Double annualCount;

    /**
     * 删除标记,0-可用，1-已删除
     */
    private Integer deleteFlag;

    /**
     * 描述
     */
    private String remark;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 创建人,当前操作人登录名称
     */
    private String createBy;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 修改人,当前操作人登录名称
     */
    private String updateBy;

    /**
     * 是否安装燃气报警器
     */
    private Integer hasAlarmDevice;

    /**
     * 报警器状态,NORMAL-正常安装, NONE-无报警器, NO_CODE-已安装,无法识别编号
     */
    private String alarmDeviceStatus;

    /**
     * 社会统一信用代码
     */
    private String creditCode;


}
