package com.test.work.datahandler.service.db2;

import com.baomidou.mybatisplus.extension.service.IService;
import com.test.work.datahandler.entity.db2.MeshBtUserExt;

/**
 * <p>
 * 瓶装气用户扩展表 服务类
 * </p>
 *
 * @author zgl
 * @since 2024-08-14
 */
public interface MeshBtUserExtService2 extends IService<MeshBtUserExt> {

}
