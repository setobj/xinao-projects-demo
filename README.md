# xinao-projects-demo

#### 介绍
新奥业务，一些demo功能

#### 软件架构
软件架构说明


#### 安装教程

1.  maven打包
    mvn package -Dmaven.test.skip=true
2.  启动
       本机执行：
       java -jar shijiazhuang-1.0-SNAPSHOT.jar --spring.profiles.active=local >/Users/zhangguoliang/Desktop/Work/IntelliJIdeaProjectsCode/xinao-projects-demo/shijiazhuang/target/shijiazhuang.log
        线上执行：
       java -jar shijiazhuang-1.0-SNAPSHOT.jar --spring.profiles.active=prod >/home/user01/temp-projects/shijiazhuang_datahandler.log

4. 测试地址
    http://localhost:9999/sjz/dataHand/matchHandlerData
    post 上传附件
4. 
    

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
